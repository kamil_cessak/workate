import React, { useEffect, useState } from "react";
import "./app.css";

export const App = () => {
  const [loading, setloading] = useState(true);
  const [photos, setphotos] = useState([]);
  const [currentStartIndex, setcurrentStartIndex] = useState(0);
  const [currentLastIndex, setcurrentLastIndex] = useState(2);
  const { wrapper, img, imgWrapper, nextButton } = styles;

  useEffect(() => {
    if (photos.length < 1 && loading) {
      fetch("https://picsum.photos/v2/list")
        .then((result) => result.json())
        .then((json) => {
          setphotos(json);
          setloading(false);
        });
    }
  });

  const formatURL = (url) => {
    let tempUrl = url.split("/");
    const len = tempUrl.length;
    return tempUrl[2] !== "unsplash.com"
      ? ""
      : "http://source.unsplash.com/" + tempUrl[len - 1];
  };

  const nextImages = () => {
    if (photos.length < currentStartIndex + 4) {
      setcurrentStartIndex(0);
      setcurrentLastIndex(2);
    } else {
      setcurrentStartIndex(currentStartIndex + 3);
      setcurrentLastIndex(currentLastIndex + 3);
    }
  };

  return (
    <main className="wrapper" style={wrapper}>
      {loading ? (
        <div>
          <h1>Ładowanie obrazków</h1>
        </div>
      ) : (
        <>
          <div style={imgWrapper}>
            {photos.map((e, i) => {
              return (
                i >= currentStartIndex &&
                i <= currentLastIndex && (
                  <img
                    key={e.id}
                    src={formatURL(e.url)}
                    alt={e.author}
                    width="30%"
                    height="400px"
                    style={img}
                  />
                )
              );
            })}
          </div>
          <button onClick={nextImages} style={nextButton}>
            Next
          </button>
        </>
      )}
    </main>
  );
};

const styles = {
  wrapper: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  nextButton: {
    display: "flex",
    marginTop: 10,
    width: 100,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    fontSize: "20px",
    border: "1px solid black",
    backgroundColor: "#fff",
  },
  img: { marginLeft: "10px", marginRight: "10px" },
  imgWrapper: {
    display: "flex",
    justifyContent: "space-between",
  },
};
